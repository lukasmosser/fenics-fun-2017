# magic eightball simulator
from random import randint


def get_input():
    return input("pose a question to the magic eightball: ")


def roll_8ball():
    idx = randint(1, 20)
    if idx == 1:
        print('\x1b[1;37;42m It is certain. \x1b[0m')
    elif idx == 2:
        print('\x1b[1;37;42m It is decidedly so. \x1b[0m')
    elif idx == 3:
        print('\x1b[1;37;42m Withou a doubt. \x1b[0m')
    elif idx == 4:
        print('\x1b[1;37;42m Yes definitely. \x1b[0m')
    elif idx == 5:
        print('\x1b[1;37;42m You may rely on it. \x1b[0m')
    elif idx == 6:
        print('\x1b[1;37;42m As I see it, yes. \x1b[0m')
    elif idx == 7:
        print('\x1b[1;37;42m Most likely. \x1b[0m')
    elif idx == 8:
        print('\x1b[1;37;42m Outlook good. \x1b[0m')
    elif idx == 9:
        print('\x1b[1;37;42m Yes. \x1b[0m')
    elif idx == 10:
        print('\x1b[1;37;42m Signs point to yes. \x1b[0m')
    elif idx == 11:
        print('\x1b[5;30;43m Reply hazy try again. \x1b[0m')
    elif idx == 12:
        print('\x1b[5;30;43m Ask again later. \x1b[0m')
    elif idx == 13:
        print('\x1b[5;30;43m Better not tell you now. \x1b[0m')
    elif idx == 14:
        print('\x1b[5;30;43m Cannot predict now. \x1b[0m')
    elif idx == 15:
        print('\x1b[5;30;43m Concentrate and ask again. \x1b[0m')
    elif idx == 16:
        print('\x1b[5;37;41m Don\'t count on it. \x1b[0m')
    elif idx == 17:
        print('\x1b[5;37;41m My reply is no. \x1b[0m')
    elif idx == 18:
        print('\x1b[5;37;41m My sources say no. \x1b[0m')
    elif idx == 19:
        print('\x1b[5;37;41m Outlook not so good. \x1b[0m')
    elif idx == 20:
        print('\x1b[5;37;41m Very doubtful. \x1b[0m')


def cont():
    chck = input("Do you want to ask another question?  \n ").lower()
    if chck[0] != 'y':
        return False
    else:
        return True


play = True
while play:
    qst = get_input()
    roll_8ball()
    play = cont()
