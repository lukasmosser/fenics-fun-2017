from random import randint

dlist = {}
wlist1 = ["pond", 
          "frog", 
          "night", 
          "day", 
          "far", 
          "warm", 
          "cold", 
          "light", 
          "dark",
          "clown",
          "sky",
          "cow",
          "crow",
          "fly",
          "death",
          "love",
          "black",
          "chill",
          "sly",
          "thin",
          "blue",
          "cat",
          "old",
          "book",
          "cloth",
          "oft",   
          "Sun",     
          "bus",
          "few",   
          "act",     
          "bid",
          "red",   
          "one",     
          "cat",
          "raid",
          "dumb",
          "Moon",
          "slew",  
          "life",    
          "base",
          "head",  
          "plot",    
          "high",
          "soul",  
          "fear",    
          "love",
          "week",  
          "head",    
          "rage",
          "base",  
          "door",    
          "drop",
          "yard",  
          "heat",    
          "pole",
          "cord",  
          "vile",    
          "path",
          "step",  
          "sack",    
          "soul",
          "week",  
          "high",    
          "fear"]

for w in wlist1:
    dlist[w] = 1

wlist2 = ["foreseen",
          "fortnight",
          "skyline",
          "tower",
          "feeling",
          "benign",
          "sailing",
          "wonder",
          "planet",
          "harder",
          "camel",
          "error",
          "object",
          "senses",
          "coldness",
          "Hiking",
          "Follow",
          "Sorrow",
          "Delay",
          "Pursue",
          "Arcade",
          "Dampen",
          "Lister",
          "Power",
          "Apple",
          "Mental",
          "Foresee",
          "Bouncing",
          "Stomach",
          "Business",
          "Forlorn",
          "Attack",
          "Barrel",
          "Cupboard",
          "Allow",
          "Porous",
          "Bigger",
          "Little",
          "Minute",
          "Yearly",
          "Plated",
          "Defeat",
          "Ghostly",
          "Teacher",
          "Iris",
          "Finger"]

for i in range(len(wlist2)):
    dlist[wlist2[i]] = 2

wlist = wlist1 + wlist2

def haiku_line(syllables):
    global wlist, dlist
    idx = randint(0, len(wlist)-1)
    word0 = wlist[idx]
    syl = dlist[word0]
    line = word0.capitalize()
    while syl < syllables:
        idx = randint(0, len(wlist)-1)
        word = wlist[idx]
        line = line + " " + word.lower()
        syl += dlist[word]
    return line

print(haiku_line(5))
print(haiku_line(7))
print(haiku_line(5))
