import datetime

year = input("Which year were you born?  ")
month = input("Which month were you born?  ")
day = input("Which day were you born?  ")

bdate = datetime.date(int(year), int(month), int(day))
now = datetime.date.today()

age = now - bdate

print("Seconds alive: ", int(age.total_seconds()))
print("")
print("Days alive:    ", int(age.total_seconds()/(3600*24)))
