# Hangman game
import csv
from random import randint
import os

# import list of words
with open("wordlist.csv") as csvfile:
    csvreader = csv.reader(csvfile, delimiter=' ', quotechar='"')
    wlist = []
    for row in csvreader:
        wlist.append("".join(row))

def get_word(wlist):
    idx = randint(0, len(wlist)-1)
    return wlist[idx].lower()


def guess_letter():
    ltr = input("Guess a letter:   ")
    return ltr.split()[0][0]


def check_letter(ltr, word, guess):
    gs = list(guess)
    if ltr in word:
        print("Great guess!")
        for i, c in enumerate(word):
            if c == ltr:
                gs[i] = c
        success = True
    else:
        print("Wrong guess...")
        success = False
    return "".join(gs), success


def cls():
    os.system('cls' if os.name=='nt' else 'clear')

cls()

print(" H A N G M A N ")
print(" A")
print(" N")
print(" G")
print(" M")
print(" A")
print(" N")

print("Welome to \"4 LETTER HANGMAN\"")
print("Try to guess the word before you hang!")

print("      _____  ")
print("      |/  |  ")
print("      |   x  ")
print("      |  /O\ ")
print("      |   \\\\ ")
print("     _|______")
print("     DON'T DIE")

hangmanpics = [
    """
    
    
    
    
    
   _______  """,
    """
    
    |
    |
    |
    |
   _|______  """,
    """
    _____
    |
    |
    |
    |
   _|______  """,
    """
    _____
    |/  
    |
    |
    |
   _|______  """,
    """
    _____
    |/  |
    |
    |
    |
   _|______  """,
    """
    _____
    |/  |
    |   x
    |
    |
   _|______  """,
    """
    _____
    |/  |
    |   x
    |   O
    |
   _|______  """,
    """
    _____
    |/  |
    |   x
    |  /O
    |
   _|______  """,
    """
    _____
    |/  |
    |   x
    |  /O\\
    |
   _|______
    """,
    """
    _____
    |/  |
    |   x
    |  /O\\
    |   \\
   _|______  """,
    """
    _____
    |/  |
    |   x
    |  /O\\
    |   \\\\
   _|______  """]



not_dead = True
play = True



while play:
    counter = 0
    max_guess = len(hangmanpics) - 1
    guess = "____"
    word = get_word(wlist)
    not_dead = True
    no_win = True
    print("Let the game begin!")
    while not_dead and no_win:
        print("Guess a letter:")
        print("(Your guess so far)  {}".format(guess))
        ltr = guess_letter()
        cls()
        guess, success = check_letter(ltr, word, guess)
        print(hangmanpics[counter])
        if not success:
            counter += 1
            print("You may still make {} mistakes".format(max_guess - counter + 1))
            if counter > max_guess:
                not_dead = False
        else:
            if guess == word:
                no_win = False
                print("You win!")
                print("You are alive after all!")
                print("Let that sink in! You are alive can do whatever you want!")
                print("Think about that!")
            print("You still have {} mistakes left".format(max_guess - counter + 1))
    if not no_win:
        print("The word is {}!".format(word))
        print("So you won the game! Amazing!")
    elif not not_dead:
        print("The word you needed to guess is: {}".format(word))
        print("You are no longer alive...")
        print("Do you want to continue playing as a zombie?")
    cont = input("Type 'play' if you want to continue: ")
    if cont.split() != "play":
        play = False
    else:
        print("Let's play another game!")
